var db = require('../helpers/db.js'),
	utils = require('../helpers/utilities.js');

module.exports = function(app){

	app
		.get('/recipes', (req, res) => {
			var collection = 'recipes',
				user = req.user;
			db.getRecords(collection, user)
				.then((results) => {
					res.send(JSON.stringify(results));
				})
				.catch(e=>{
					utils.error(res, e);
				});
		})
		.get('/recipes/:id', (req, res) => {
			var collection = 'recipes',
				user = req.user,
				id = req.params.id;

			var	find = { '_id': utils.objectId(id), 'userId': utils.objectId(user)};
			db.getRecordBy(collection, find)
				.then((results) => {
					res.send(results);
				})
				.catch(e=>{
					utils.error(res, e);
				});
		})
		.post('/recipes', (req, res) => {
			var user = req.user,
				recipe = utils.parseUpload(req.body,res,'new');
			if(!recipe)return;
			recipe.userId = utils.objectId(user);
			db.insertRecord('recipes', recipe)
				.then((results) => {
					res.status(201).send(results[0]);
				})
				.catch(e=>{
					utils.error(res, e);
				});
		})
		.put('/recipes/:id', (req, res) => {
			var id = req.params.id,
				user = req.user,
				recipe = utils.parseUpload(req.body,res);
			if(!recipe)return;
			delete recipe._id;
			delete recipe.userId;

			var find = { '_id': utils.objectId(id), 'userId': utils.objectId(user) };
			db.updateRecord('recipes', find, recipe)
				.then(() => {
					recipe._id = id;
					recipe.userId = user;
					res.send(recipe);
				})
				.catch(e=>{
					utils.error(res, e);
				});
		})
		.delete('/recipes', (req, res) => {
			var user = req.user,
				collection = 'recipes',
				recipes = req.body;

			var promises = [];
			recipes.forEach(function(id){
				var find = { '_id': utils.objectId(id), 'userId': utils.objectId(user) };
				promises.push(db.deleteRecord(collection, find)
					.then(()=>{
						utils.deletePhotos(id);
					})
					.catch(e=>{
						utils.error(res, e);
					}));
			});
			Promise.all(promises)
				.then(()=>{
					res.status(204).send();
				});
		})
		.delete('/recipes/:id', (req, res) => {
			var id = req.params.id,
				user = req.user,
				collection = 'recipes';
			var find = { '_id': utils.objectId(id), 'userId': utils.objectId(user) };
			db.deleteRecord(collection, find)
				.then(()=>{
					utils.deletePhotos(id);
					res.status(204).send();
				})
				.catch(e=>{
					utils.error(res, e);
				});
		});
};
