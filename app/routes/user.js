var db = require('../helpers/db.js'),
	utils = require('../helpers/utilities.js'),
	bcrypt = require('bcrypt-nodejs'),
	jwt = require('jsonwebtoken'),
	secret = require('../config/jwtSecret.js');


module.exports = function(app){

	function userExists(email,callback){
		var find = { 'email': email };
		db.getRecordBy('users', find, callback);
	}
	function insertUser(res,user){
		db.insertRecord('users', user, function (err,result) {
			if (err) utils.error(res, err);
			res.send({ result: result });
		});
	}
	function validPassword(password,hash){
		return bcrypt.compareSync(password, hash);
	}
	function generateHash(password){
		return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
	}

	app
		.post('/register', (req, res) => {
			var user = req.body;
			if (user.password != user.passwordConfirm) {
				utils.error(res,'Passwords don\'t match');
			} else {
				userExists(user.email, (err, results) => {
					if (err) utils.error(res, err);
					if (!results) {
						var newUser = user;
						newUser.password = generateHash(user.password);
						delete newUser.passwordConfirm;
						delete newUser.emailConfirm;
						insertUser(res, newUser);
					} else {
						utils.error(res, 'Email already exists in database');
					}
				});
			}
		})
		.post('/login', (req, res) => {
			var user = req.body;
			userExists(user.email, (err, results) => {
				if (err) utils.error(res, err);
				if (!results) {
					utils.error(res,'Invalid login details');
				} else {
					var valid = validPassword(user.password, results.password);
					if (valid) {
						var id = typeof results._id == 'object' ? results._id.toString() : results._id;
						var token = jwt.sign(id, secret.JWTsecret);
						res.send({ token: token });

					} else {
						utils.error(res,'Invalid login details');
					}
				}
			});
		})
		.put('/user-update', (req, res) => {
			var data = req.body;
			var find = { '_id': utils.objectId(data.user) };
			var oldPassword = data.user.passwordOld;
			var password = generateHash(data.user.passwordNew);
			db.getRecordBy('users', find, (err, result) => {
				if (result) {
					var valid = validPassword(oldPassword, result.password);
					if (valid) {
						var set = { 'password': password };
						db.updateRecord('users', find, set, (err,result) => {
							if (err) utils.error(res, err);
							res.send({ result: result });
						});
					} else {
						utils.error(res, 'Invalid old password');
					}
				} else {
					utils.error(res, 'User not found');
				}
			});

		});

};
