var db = require('../helpers/db.js'),
	utils = require('../helpers/utilities.js');

module.exports = function(app){

	app.get('/list', function (req, res) {
		var collection = 'list',
			user = req.user;
		db.getRecords(collection, user, function (err,results) {
			if (err) utils.error(res, err);
			res.send(JSON.stringify(results));
		});
	});
	app.post('/list', function (req, res) {
		var user = req.user;
		var list = JSON.parse(req.body.list);
		var total = list.length;
		var resultsArray = [];
		for (var i = 0; i < total; i++) {
			var listItem = list[i];
			listItem.userId = utils.objectId(user);
			db.insertRecord('list', listItem, function (err, results) {
				if (err) utils.error(res, err);
				resultsArray.push(results);
				if (resultsArray.length == total) {
					res.send(resultsArray);
				}
			});
		}
	});
	app.put('/list', function (req, res) {
		var data = req.body;
		var ingredient = data.ingredient;
		var bought = data.bought;
		var find = { 'name': ingredient, 'userId': utils.objectId(req.user) };
		var set = { 'bought': bought };
		db.updateRecord('list', find, set, function (err,results) {
			if (err) utils.error(res, err);
			res.send({ results: results });
		});
	});
	app.delete('/list', function (req, res) {
		var find = { 'userId': utils.objectId(req.user) };
		db.deleteRecord('list', find, function (err, results) {
			if (err) utils.error(res, err);
			res.send({ results: results });
		});
	});

};
