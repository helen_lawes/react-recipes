var express = require('express'),
	expressjwt = require('express-jwt'),
	secret = require('./config/jwtSecret.js'),
	MultiParser = require('./helpers/multiparser.js'),
	bodyParser = require('body-parser'),
	session = require('express-session');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var publicFolder = __dirname + '/public';
var indexFile = publicFolder+'/index.html';

app.use(function(req, res, next) {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	res.header('Access-Control-Allow-Headers', 'X-Requested-With');
	next();
});
app.use(['/api/recipes(/:id)?','/api/list','/api/user-update'], expressjwt({ secret: secret.JWTsecret }));
app.use(function (err, req, res, next) {
	if (err.name === 'UnauthorizedError') {
		res.status(401).send('Access denied');
	}
	next();
});
app.use(session({
	secret:'recipes-secret',
	resave: false,
	saveUninitialized: true
}));
app.use(function (req, res, next) {
	if(req.user){
		req.session.userId  = req.user;
	}
	next();
});
app.use(MultiParser());
app.use(bodyParser.json());
app.use(express.static(publicFolder));
app.use(express.static(__dirname + '/../node_modules'));

var api = express();
require('./routes/recipes.js')(api);
require('./routes/user.js')(api);
require('./routes/list.js')(api);

app.use('/api',api);
app.use(['/recipes(/:id)?','/login','/logout','/register','/list'],function (req,res){
	res.sendFile(indexFile);
});

server.listen(8080);

io.on('connection',function(socket){
	socket.emit('message','connected');
});
