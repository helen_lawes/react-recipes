var ObjectID = require('mongodb').ObjectID,
	fs = require('fs'),
	path = require('path');

var imageDir = path.normalize(__dirname + '/../../public/images/');
module.exports = {
	error: function(res,err,errCode){
		if(!errCode) errCode = 500;
		res.status(errCode).send(err);
	},
	objectId: function(){
		return ObjectID.apply(this,arguments);
	},
	parseUpload: function(data, res, newRecipe){
		var recipe = JSON.parse(data.recipe);
		var file = data.file;
		var bigError = false;
		if(newRecipe) {
			recipe._id = this.objectId(recipe._id);
			if(file && typeof file.name != 'undefined'){
				var filename = file.name.split('.');
				recipe.photo = recipe._id+'.'+ filename[filename.length-1]+'?'+new Date().getTime();
			}
		}
		if (file && typeof file.name != 'undefined') {
			var recipePhoto = recipe.photo.split('?');
			var newFile = imageDir + recipePhoto[0];
			try{
				var is = fs.createReadStream(file.path);
				var os = fs.createWriteStream(newFile);
				is.pipe(os);
				is.on('end', function(){
					fs.unlinkSync(file.path);
				});
			} catch (e){
				this.error(res,e);
				bigError = true;
			}
		}
		return bigError ? false : recipe;
	},
	deletePhotos: function(id){
		var glob = require('glob');
		glob(imageDir+id+'*', function(err,files){
			files.forEach(function(file){
				fs.unlinkSync(file);
			});
		});
	}
};
