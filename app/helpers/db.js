let MongoClient = require('mongodb').MongoClient,
	ObjectID = require('mongodb').ObjectID,
	MDB = require('../config/db');


module.exports = {

	_db: null,

	_connection: `mongodb://${MDB.host}:${MDB.port}/${MDB.name}`,

	_connect: function(){
		return MongoClient.connect(this._connection)
			.then((database)=>{
				this._db = database;
				this._db.on('close',()=>{
					this._db = null;
				});
			});
	},

	_getDb: function(){
		return new Promise((resolve,reject)=>{
			if(!this._db){
				this._connect()
					.then(()=>{
						resolve(this._db);
					})
					.catch(reject);
			} else{
				resolve(this._db);
			}
		});
	},

	_closeDb: function() {
		if(this._db) this._db.close();
	},

	getRecords: function (collection, user, callback = function(){}) {
		return this._getDb()
			.then(db=>{
				let coll = db.collection(MDB.collections[collection]);
				return coll.find({ 'userId': ObjectID(user) }).toArray();
			})
			.then((results)=>{
				this._closeDb();
				callback(null,results);
				return results;
			})
			.catch(e=>{
				callback(e);
			});
	},

	getRecordBy: function (collection, find, callback = function(){}) {
		return this._getDb()
			.then(db=>{
				let coll = db.collection(MDB.collections[collection]);
				return coll.findOne(find);
			})
			.then((results)=>{
				this._closeDb();
				callback(null,results);
				return results;
			})
			.catch(e=>{
				callback(e);
			});
	},

	insertRecord: function (collection, data, callback = function(){}) {
		return this._getDb()
			.then(db=>{
				let coll = db.collection(MDB.collections[collection]);
				return coll.insert(data, { w: 1 });
			})
			.then((results)=>{
				this._closeDb();
				callback(null,results.ops);
				return results.ops;
			})
			.catch(e=>{
				callback(e);
			});
	},

	updateRecord: function (collection, find, data, callback = function(){}) {
		return this._getDb()
			.then(db=>{
				let coll = db.collection(MDB.collections[collection]);
				return coll.update(find, { $set: data }, { w: 1 });
			})
			.then((results)=>{
				this._closeDb();
				callback(null,results);
				return results;
			})
			.catch(e=>{
				callback(e);
			});

	},

	deleteRecord: function (collection, find, callback = function(){}) {
		return this._getDb()
			.then(db=>{
				let coll = db.collection(MDB.collections[collection]);
				return coll.remove(find);
			})
			.then((results)=>{
				this._closeDb();
				callback(null,results);
				return results;
			})
			.catch(e=>{
				callback(e);
			});
	}

};
