let MongoClient = require('mongodb').MongoClient,
	MDB = require('../config/db');

let connection = `mongodb://${MDB.host}:${MDB.port}/${MDB.name}`;
console.log(`Database connection: ${connection}`); //eslint-disable-line

let db;

MongoClient.connect(connection)
	.then((database)=>{
		db = database;
		console.log('Connected to database'); //eslint-disable-line
		let promises = [];
		for( let name in MDB.collections ){
			promises.push(db.createCollection(`${MDB.collections[name]}`));
			console.log(`Added collection: ${MDB.collections[name]}`); //eslint-disable-line
		}
		return Promise.all(promises);
	})
	.then(()=>{
		console.log('Created all collections'); //eslint-disable-line
		db.close();
		console.log('All operations completed without error.'); //eslint-disable-line
	})
	.catch(e=>{
		console.error(e); //eslint-disable-line
	});
