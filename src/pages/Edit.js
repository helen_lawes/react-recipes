import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { updateRecipe } from '../actions';

import RecipeEdit from '../components/RecipeEdit/RecipeEdit';
import ActionBar from '../components/ActionBar/ActionBar';
import { IconButton } from '../components/Buttons';

class Edit extends React.Component {

	render() {
		let { recipe } = this.props;
		return (
			<React.Fragment>
				<ActionBar>
					<IconButton to={`/recipe/${recipe && recipe._id}`}>arrow_back</IconButton>
					<h1>Recipe</h1>
				</ActionBar>
				{recipe && <RecipeEdit {...recipe} returnUrl={`/recipe/${recipe._id}`} saveRecipe={this.props.saveRecipe} />}
			</React.Fragment>
		);
	}
}

Edit.propTypes = {
	recipe: PropTypes.object,
	saveRecipe: PropTypes.func.isRequired,
	match: PropTypes.object,
};

const mapStateToProps = (state, ownProps) => ({
	recipe: state.recipes.find(recipe => recipe._id === ownProps.match.params.id),
});

const mapDispatchToProps = dispatch => ({
	saveRecipe: payload => dispatch(updateRecipe(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Edit);
