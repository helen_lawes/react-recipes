import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createRecipe } from '../actions';

import RecipeEdit from '../components/RecipeEdit/RecipeEdit';
import ActionBar from '../components/ActionBar/ActionBar';
import { IconButton } from '../components/Buttons';

class Edit extends React.Component {

	render() {
		return (
			<React.Fragment>
				<ActionBar>
					<IconButton to="/">arrow_back</IconButton>
					<h1>Recipe</h1>
				</ActionBar>
				<RecipeEdit returnUrl="/" saveRecipe={this.props.saveRecipe} />
			</React.Fragment>
		);
	}
}

Edit.propTypes = {
	saveRecipe: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => ({
	saveRecipe: payload => dispatch(createRecipe(payload)),
});

export default connect(null, mapDispatchToProps)(Edit);
