import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import bindMethods from 'yaab';
import { deleteRecipe } from '../actions';

import RecipeDetails from '../components/RecipeDetails/RecipeDetails';
import ActionBar from '../components/ActionBar/ActionBar';
import { IconButton } from '../components/Buttons';
import Confirm from '../components/Confirm/Confirm';

class Recipe extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			confirmDelete: false,
		};
		bindMethods(this);
	}

	confirmDelete() {
		this.setState({
			confirmDelete: true,
		});
	}

	hideDeleteDialog() {
		this.setState({
			confirmDelete: false,
		});
	}

	deleteRecipe() {
		this.hideDeleteDialog();
		this.props.deleteRecipe(this.props.recipe);
	}

	render() {
		let { recipe } = this.props;
		return (
			<React.Fragment>
				<ActionBar>
					<IconButton to="/">arrow_back</IconButton>
					<h1>Recipe</h1>
					<IconButton onClick={this.confirmDelete}>delete</IconButton>
				</ActionBar>
				{recipe && <RecipeDetails {...recipe} editUri={`/recipe/${recipe._id}/edit`} />}
				{this.state.confirmDelete && <Confirm onConfirm={this.deleteRecipe} onCancel={this.hideDeleteDialog}>Delete recipe?</Confirm> }
			</React.Fragment>
		);
	}
}

Recipe.propTypes = {
	recipe: PropTypes.object,
	match: PropTypes.object,
	deleteRecipe: PropTypes.func,
};

const mapStateToProps = (state, ownProps) => ({
	recipe: state.recipes.find(recipe => recipe._id === ownProps.match.params.id),
});

const mapDispatchToProps = dispatch => ({
	deleteRecipe: payload => dispatch(deleteRecipe(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Recipe);
