import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import bindMethods from 'yaab';
import { Redirect } from 'react-router-dom';

import history from '../utils/history';
import { loginUser, registerUser, clearErrorGroup } from '../actions';
import Login from '../components/Login/Login';

class Security extends React.Component {
	constructor() {
		super();
		this.state = {
			register: !!history.location.pathname.match('register'),
		};
		bindMethods(this);
	}

	render() {
		let { register } = this.state;
		return (
			this.props.recipes.length ?
				<Redirect to="/" /> : 
				<Login submit={register ? this.props.register : this.props.login} register={register} errors={this.props.errors} clearErrors={this.props.clearErrors} />
		);
	}
}

Security.propTypes = {
	login: PropTypes.func,
	register: PropTypes.func,
	recipes: PropTypes.array,
	errors: PropTypes.array,
	clearErrors: PropTypes.func,
};

const mapStateToProps = state => ({
	recipes: state.recipes,
	errors: state.errors.filter(error => error.group === 'user'),
});

const mapDispatchToProps = dispatch => ({
	login: user => dispatch(loginUser(user)),
	register: user => dispatch(registerUser(user)),
	clearErrors: () => dispatch(clearErrorGroup('user')),
});

export default connect(mapStateToProps, mapDispatchToProps)(Security);
