import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styled from 'styled-components';

import RecipeBlock from '../components/RecipeBlock/RecipeBlock';
import { FabButton } from '../components/Buttons';
import ActionBar from '../components/ActionBar/ActionBar';
import { getColor } from '../utils/styles';

const Recipes = styled.div`
	display: flex;
	flex-wrap: wrap;
	justify-content: center;
	margin:60px -15px 20px 0;
`;

const NoRecipes = styled.div`
	text-align: center;
	color: ${getColor('primary')};
	position: absolute;
	top: 50%;
	left: 0;
	width: 100%;
	font-size: 25px;
`;

class Home extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		let { recipes } = this.props;
		return (
			<React.Fragment>
				<ActionBar>
					<h1>Recipe Book</h1>
				</ActionBar>
				<Recipes>
					{recipes && recipes.length > 0 ? 
						recipes.map( (recipe, i) => <RecipeBlock {...recipe} to={`/recipe/${recipe._id}`} key={i} />) 
						:
						<NoRecipes>No recipes found</NoRecipes>
					}
				</Recipes>
				<FabButton className="create-new" aria-label="Create new" to="/recipe/new" fixed bottomCorner>add</FabButton>
			</React.Fragment>
		);
	}
}

Home.propTypes = {
	recipes: PropTypes.array.isRequired,
};

const mapStateToProps = state => ({
	recipes: state.recipes,
});

export default connect(mapStateToProps, null)(Home);
