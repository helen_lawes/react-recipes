import { css } from 'styled-components';

// colour palette
const $colors = {
	'light': '#fff',
	'hint': '#eceff1',
	'border': '#CFD8DC',
	'main': '#263238',
	'footer': '#37474f',
	'tags': '#90A4AE',
	'dark-header': '#263238',
	'dark': '#000',
	'primary-dark': '#455A64',
	'primary': '#607D8B',
	'primary-light': '#CFD8DC',
	'icon': '#FFFFFF',
	'accent': '#FF4081',
	'accent-light': '#ff80ab',
	'accent-dark': '#F50057',
	'primary-text': '#212121',
	'secondary-text': '#757575',
	'divider': '#e5eef2',
	'notification': '#323232',
};

export const getColor = name => $colors[name];

const parse = (string, offset, short) => {
	let multiplier = (short ? 1 : 2);
	let substring = string.substring(offset * multiplier, offset * multiplier + multiplier);
	substring += short ? substring : '';
	return parseInt(substring, 16);
};

export function hex2Rgba(hex, alpha) {
	hex = hex.replace('#','');
	const short = hex.length === 3;
	const r = parse(hex, 0, short);
	const g = parse(hex, 1, short);
	const b = parse(hex, 2, short);
	return `rgba(${r}, ${g}, ${b}, ${alpha})`;
}

export const mixins = {
	placeholder: (content) => {
		return `::-webkit-input-placeholder {
			${content}
		}

		:-moz-placeholder { /* Firefox 18- */
			${content}
		}

		::-moz-placeholder {  /* Firefox 19+ */
			${content}
		}

		:-ms-input-placeholder {
			${content}
		}`;
	},
};

const sizes = {
	medium: 700,
	small: 500,
};

// Iterate through the sizes and create a media template
export const media = Object.keys(sizes).reduce((acc, label) => {
	acc[label] = (...args) => css`
		@media (max-width: ${sizes[label] / 16}em) {
			${css(...args)}
		}
	`;

	return acc;
}, {});

export const placeholders = {
	input: `
		font-family:inherit;
		font-size: 16px;
		height: 28px;
		border:0;
		border-bottom:1px solid ${getColor('hint')};
		color:${getColor('primary-text')};
		background:none;
		&:focus{
			border-bottom: 2px solid ${getColor('accent')};
			outline:none;
		}
	`,
	fontNormal: `
		font-weight: 400;
	`,
	cursiveFont: `
		font-family: 'Shadows Into Light', cursive;
	`,
	dash: `
		stroke-dasharray: 1500;
		stroke-dashoffset: 1500;
	`,
	boxShadow: `
		box-shadow: 0 2px 2px 0 rgba(0,0,0,.14),0 3px 1px -2px rgba(0,0,0,.2),0 1px 5px 0 rgba(0,0,0,.12);
	`,
	elevatedBoxShadow: `
		box-shadow: 0 8px 8px 0 rgba(0,0,0,.28),0 3px 1px -2px rgba(0,0,0,.4),0 1px 5px 0 rgba(0,0,0,.12);
	`,
	transition: `
		transition: 0.25s;
	`,
	box: `
		max-width:100%;
		width:600px;
		border-radius: 2px;
		background:${getColor('light')};
	`,
};
