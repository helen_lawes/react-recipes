export const truncate = (v, length) => {
	if(typeof v === 'undefined'||!v) return '';
	if(typeof v === 'string'){
		var parts = v.split(' ');
		var string = '';
		var temp = '';
		for(var i = 0; i < parts.length; i++){
			temp += ` ${parts[i]}`;
			if(temp.length < length){
				string = temp;
			} else {
				return string.substr(1) + '...';
			}
		}
		return string.substr(1);
	}
	return v;
};
