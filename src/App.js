import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Router, Route } from 'react-router-dom';

import history from './utils/history';
import { getAllRecipes } from './actions';

import Home from './pages/Home';
import Security from './pages/Security';
import Recipe from './pages/Recipe';
import Edit from './pages/Edit';
import New from './pages/New';

import StyledApp from './App.styles';

class App extends Component {

	componentDidMount() {
		this.props.getAll();
	}

	render() {
		return (
			<StyledApp>
				<Router history={history}>
					<div>
						<Route exact path="/" component={Home} />
						<Route exact path="/recipe/:id" component={Recipe} />
						<Route exact path="/recipe/:id/edit" component={Edit} />
						<Route exact path="/recipe/new" component={New} />
						<Route exact path="/login" component={Security} />
						<Route exact path="/register" component={Security} />
					</div>
				</Router>
			</StyledApp>
		);
	}
}

App.propTypes = {
	getAll: PropTypes.func.isRequired,
	recipes: PropTypes.array.isRequired,
};

const mapStateToProps = state => ({
	recipes: state.recipes,
});

const mapDispatchToProps = dispatch => ({
	getAll: () => dispatch(getAllRecipes())
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
