import React from 'react';
import PropTypes from 'prop-types';
import bindMethods from 'yaab';
import styled from 'styled-components';

import { getColor } from '../../utils/styles';
import { TextField } from '../Form';
import { IconButton } from '../Buttons';

const Method = styled.div`
	display: flex;
	margin-bottom:20px;
	.number{
		width:20px;
		padding-top: 2px;
		color:${getColor('secondary-text')};
	}
`;


class MethodsEdit extends React.Component {
	constructor() {
		super();
		this.state = {
			methods: []
		};
		bindMethods(this);
	}

	static getDerivedStateFromProps(props) {
		let { methods } = props;
		return { methods };
	}

	handleChange(index) {
		return event => {
			let value = event.target.value;
			this.setState(({ methods }) => {
				methods[index].value = value;
				return {
					methods,
				};
			}, () => {
				this.props.onChange(this.state.methods);
			});
		};
	}

	handleDelete(index) {
		return event => {
			event.preventDefault();
			this.setState(({ methods }) => {
				methods.splice(index, 1);
				return {
					methods,
				};
			}, () => {
				this.props.onChange(this.state.methods);
			});
		};
	}

	render() {
		let { methods } = this.state;
		return (
			<React.Fragment>
				{ methods && methods.map( (method, i) => {
					return (
						<Method key={i}>
							<span className="number">{i+1}</span>
							<TextField value={method.value} multiline onChange={this.handleChange(i)} />
							<IconButton className="delete" onClick={this.handleDelete(i)}>delete</IconButton>
						</Method>
					);
				}) }
			</React.Fragment>
		);
	}
}

MethodsEdit.propTypes = {
	methods: PropTypes.array,
	onChange: PropTypes.func,
};

export default MethodsEdit;
