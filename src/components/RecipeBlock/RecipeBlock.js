import React from 'react';
import PropTypes from 'prop-types';
import StyledRecipeBlock from './RecipeBlock.styles';
import { withRouter } from 'react-router-dom';

import { truncate } from '../../utils/formatting';

const RecipeBlock = ({ photo, brief_description, name, to, history }) => (
	<StyledRecipeBlock onClick={() => history.push(to)}>
		<div className="recipe-image"><img src={`images/${photo}`} alt="" /></div>
		<h2 className={`${brief_description && 'short'}`} title={name}>{name}</h2>
		{ brief_description && 
			<div className="recipe-brief-description">
				{truncate(brief_description, 70)}
			</div> 
		}
	</StyledRecipeBlock>
);

RecipeBlock.propTypes = {
	photo: PropTypes.string,
	brief_description: PropTypes.string,
	name: PropTypes.string,
	to: PropTypes.string,
	history: PropTypes.any,
};

export default withRouter(RecipeBlock);
