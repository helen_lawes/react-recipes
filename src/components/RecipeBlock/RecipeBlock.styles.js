import styled from 'styled-components';

import { placeholders, media, getColor } from '../../utils/styles';

const StyledRecipeBlock = styled.div`
	${placeholders.boxShadow}
	${placeholders.box}
	${placeholders.transition}
	width:220px;
	margin: 15px 15px 5px 0;
	overflow:hidden;
	cursor: pointer;
	&:focus,
	&:hover{
		${placeholders.elevatedBoxShadow}
	}
	${media.small`
		width:100%;
	`}
	h2{
		${placeholders.cursiveFont}
		line-height: 1.3em;
		padding:15px;
		margin:0;
		&.short{
			white-space: nowrap;
			text-overflow: ellipsis;
			overflow: hidden;
			${media.small`
				white-space: normal;
				overflow: auto;
			`}
		}
	}
	.recipe-image{
		height:160px;
		overflow: hidden;
		border-bottom:1px solid ${getColor('divider')};
	}
	.recipe-brief-description {
		padding: 10px 20px 20px;
		color: #757575;
		font-size: 14px;
	}
	img{
		max-width: 140%;
		min-height:100%;
	}
`;

export default StyledRecipeBlock;
