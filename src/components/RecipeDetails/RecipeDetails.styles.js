import styled from 'styled-components';

import { media, placeholders, getColor } from '../../utils/styles';

const StyledRecipeDetails = styled.div`
	.button-group{
		text-align: right;
		position: fixed;
		bottom: 20px;
		left: 50%;
		width: 300px;
		max-width: 50%;
		margin-left: 29px;
		${media.medium`
			width:auto;
			left:auto;
			right:20px;
			margin-left: 0;
		`}
		.fab-button{
			position: relative;
			z-index: 2;
		}
	}
	.recipe-photo{
		position: absolute;
		top: 39px;
		left: 0;
		width: 100%;
		overflow: hidden;
		img{
			width:100%;
			${media.small`
				width: 150%;
				max-width: none;
				margin: 0 -7%;
			`}
		}
	}
	.recipe-description{
		${placeholders.boxShadow}
		${placeholders.box}
		width:600px;
		margin:200px auto 50px;
		padding:20px;
		position: relative;
		h2{
			${placeholders.cursiveFont}
			font-size: 2em;
			line-height: 1.3em;
			margin-top: 10px;
		}
	}
	.ingredients{
		margin-bottom:30px;
	}
	.ingredient{
		margin-top:10px;
	}
	.link{
		word-wrap: break-word;
	}
	.icon{
		vertical-align: bottom;
	}
	.note,
	.tags{
		color:${getColor('secondary-text')};
		font-size: 13px;
		text-indent: -29px;
		padding-left: 30px;
		.icon{
			position: relative;
			left:10px;
		}
	}
	.divider{
		margin-right: -20px;
		margin-left: -20px;
	}
	.properties{
		margin:30px 0 20px;
		> span{
			display: inline-block;
			margin-bottom:10px;
			vertical-align: middle;
		}
	}
	.people,
	.timer{
		margin-right:20px;
	}
	h3, h4{
		font-size: 16px;
		color:${getColor('accent-dark')};
		margin:0 0 20px 0;
	}
	h4{
		font-size: 14px;
		color:${getColor('secondary-text')};
		margin-top: 20px;
	}
	ol{
		padding-left: 20px;
		margin-bottom:30px;
		li{
			margin-top:20px;
		}
	}

`;

export default StyledRecipeDetails;
