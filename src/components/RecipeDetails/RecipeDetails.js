import React from 'react';
import PropTypes from 'prop-types';

import StyledRecipeDetails from './RecipeDetails.styles';
import IngredientsList from '../IngredientsList/IngredientsList';
import { FabButton } from '../Buttons';

const RecipeDetails = ({ photo, name, brief_description, time, category, serves, tags, personal_notes, ingredients, methods, recipe_refs, editUri }) => (
	<StyledRecipeDetails>
		<div className="recipe recipe-detail">
			<div className="recipe-photo"><img src={`/images/${photo}`} alt="" /></div>
			<div className="recipe-description">
				<div className="button-group">
					<FabButton className="edit-button" aria-label={`Edit ${name}`} to={editUri}>edit</FabButton>
				</div>
				<h2>{name}</h2>
				<p>{brief_description}</p>
				<div className="properties">
					{ time>0 && 
						<span className="timer"><span className="icon">timer</span> {time}</span>
					}
					{ serves>0 && 
						<span className="people"><span className="icon">restaurant_menu</span> {serves}</span>
					}
					{ category && 
						<span><span className="icon">filter</span> {category}</span>
					}
				</div>
				{ tags && tags.length > 0 && tags[0]!=='' &&
					<p className="tags"><span className="icon">local_offer</span> {tags.join(', ')}</p>
				}
				{ personal_notes && 
					<p className="note"><span className="icon">note</span> {personal_notes}</p>
				}				
				{ ingredients && <IngredientsList ingredients={ingredients} /> }
				{ methods && methods.length > 0 && methods[0].value &&
					<div className="methods">
						<div className="divider"></div>
						<h3>Method steps</h3>
						<ol>
							{ methods.map( (method, i) => <li key={i}>{method.value}</li> ) }
						</ol>
					</div>
				}
				{ recipe_refs &&
					<p className="link">
						<a href={recipe_refs} target="_blank" rel="noopener noreferrer">{recipe_refs}</a>
					</p>
				}
			</div>
		</div>
	</StyledRecipeDetails>
);

RecipeDetails.propTypes = {
	photo: PropTypes.string,
	name: PropTypes.string,
	brief_description: PropTypes.string,
	time: PropTypes.string,
	category: PropTypes.string,
	serves: PropTypes.string,
	tags: PropTypes.array,
	personal_notes: PropTypes.string,
	ingredients: PropTypes.array,
	methods: PropTypes.array,
	recipe_refs: PropTypes.string,
	editUri: PropTypes.string,
};

export default RecipeDetails;



