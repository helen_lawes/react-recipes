import styled from 'styled-components';

import { placeholders } from '../../utils/styles';

const StyledConfirm = styled.div`
	${placeholders.boxShadow}
	${placeholders.box}
	position: fixed;
	top: 20px;
	right: 20px;
	width:300px;
	padding: 20px;
	max-width: calc(100% - 40px);
	z-index: 5;
`;

export const ConfirmFooter = styled.div`
	padding-top: 20px;
	text-align: right;
`;

export default StyledConfirm;
