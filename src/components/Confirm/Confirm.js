import React from 'react';
import PropTypes from 'prop-types';

import StyledConfirm, { ConfirmFooter } from './Confirm.styles';
import { Button } from '../Buttons';

const Confirm = ({ children, cancelButton, confirmButton, onCancel, onConfirm }) => (
	<StyledConfirm role="dialog">
		<div>
			{children}
		</div>
		<ConfirmFooter>
			<Button onClick={onCancel}>{cancelButton}</Button>
			<Button onClick={onConfirm}>{confirmButton}</Button>
		</ConfirmFooter>
	</StyledConfirm>
);

Confirm.defaultProps = {
	cancelButton: 'Cancel',
	confirmButton: 'Confirm'
};

Confirm.propTypes = {
	children: PropTypes.node,
	onCancel: PropTypes.func,
	onConfirm: PropTypes.func,
	cancelButton: PropTypes.string,
	confirmButton: PropTypes.string,
};

export default Confirm;
