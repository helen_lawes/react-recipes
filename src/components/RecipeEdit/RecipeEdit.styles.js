import styled from 'styled-components';

import { placeholders, media, getColor } from '../../utils/styles';

const StyledRecipeEdit = styled.form`
	${placeholders.boxShadow}
	${placeholders.box}
	width:600px;
	margin:76px auto 50px;
	padding:20px;
	.actions{
		margin-top:40px;
	}
	.ingredients{
		margin-top:50px;
		margin-bottom: 25px;
		${media.small`
			.last{
				margin-bottom: 0;
			}
		`}
	}
	.add-ingredient{
		margin-top: 10px;
	}
	.delete{
		cursor: pointer;
	}
	.methods{
		margin-top:50px;
	}
	.save {
		position: fixed;
		top: 20px;
		right: 20px;
		z-index: 6;
		&.icon {
			color: ${getColor('light')};
		}
	}
`;

export default StyledRecipeEdit;
