import React from 'react';
import PropTypes from 'prop-types';
import bindMethods from 'yaab';
import { withRouter } from 'react-router-dom';

import StyledRecipeEdit from './RecipeEdit.styles';

import IngredientsEdit from '../IngredientsEdit/IngredientsEdit';
import MethodsEdit from '../MethodsEdit/MethodsEdit';
import { FieldContainer, TextField, Label, Select, File } from '../Form';
import { Button, IconButton } from '../Buttons';

class RecipeEdit extends React.Component {

	constructor() {
		super();
		this.state = {
			name: '',
			brief_description: '',
			time: '',
			category: '',
			serves: '',
			tags: [],
			personal_notes: '',
			ingredients: [],
			methods: [],
			recipe_refs: '',
			photo: '',
			file: {
				name: ''
			},
		};
		bindMethods(this);
	}

	componentDidMount() {
		let { 
			name = '',
			brief_description = '',
			time = '',
			category = '',
			serves = '',
			tags = [],
			personal_notes = '',
			ingredients = [],
			methods = [],
			recipe_refs = '',
			photo = '',
		} = this.props || {};
		this.setState({ 
			name,
			brief_description,
			time,
			category,
			serves,
			tags,
			personal_notes,
			ingredients,
			methods,
			recipe_refs,
			photo,
		});
	}

	handleChange(name, toArray = false) {
		return event => {
			let value = event.target ? event.target.value : event;
			if (toArray) {
				value = value.split(',').map(v => v.trim());
			}
			this.setState({
				[name]: value,
			});
		};
	}

	fileChange(event){
		let file = event.target.files[0];
		if (file) {
			let filename = file.name.split('.');
			let photo = `${this.props._id}.${filename[filename.length-1]}?${new Date().getTime()}`;
			this.setState({
				file,
				photo,
			});
		} else {
			file = { value: '' };
			this.setState({
				file,
			});
		}
	}

	addIngredient(event) {
		event.preventDefault();
		this.setState(state => ({
			ingredients: [
				...state.ingredients,
				{}
			]
		}));
	}

	addMethod(event) {
		event.preventDefault();
		this.setState(state => ({
			methods: [
				...state.methods,
				{ value: '' }
			]
		}));
	}

	save(event) {
		event.preventDefault();
		let recipe = { ...this.state, _id: this.props._id };
		this.props.saveRecipe(recipe);
		this.props.history.push(this.props.returnUrl);
	}

	toString(array) {
		return array.join(',');
	}

	render() {
		let { name, brief_description, time, category, serves, tags, personal_notes, ingredients, methods, recipe_refs, file, photo } = this.state;
		return (
			<StyledRecipeEdit autoComplete="off">
				<TextField value={name} label="Name" className="recipe-name" id="name" onChange={this.handleChange('name')} large padded />
				<File label="Photo" id="file" padded value={file.name||photo} onChange={this.fileChange} />
				<TextField type="textarea" value={brief_description} label="Brief description" onChange={this.handleChange('brief_description')} padded multiline />
				<FieldContainer padded>
					<Label htmlFor="category" above={true}>Category</Label>
					<Select value={category} onChange={this.handleChange('category')} id="category">
						<option value="">(None)</option>
						<option value="starter">Starter</option>
						<option value="main">Main</option>
						<option value="dessert">Dessert</option>
						<option value="cake">Cake/Baking</option>
						<option value="snack">Snack</option>
					</Select>
				</FieldContainer>

				<TextField value={this.toString(tags)} label="Tags" className="row" id="tags" onChange={this.handleChange('tags', true)} padded />
				<TextField type="number" value={time} label="Time" className="row" id="time" onChange={this.handleChange('time')} padded />
				<TextField type="number" value={serves} label="Serves" className="row" id="serves" onChange={this.handleChange('serves')} padded />
				<TextField type="textarea" value={personal_notes} label="Personal notes" className="row" id="personal_notes" onChange={this.handleChange('personal_notes')} padded />
				<TextField value={recipe_refs} label="References" className="row" id="references" onChange={this.handleChange('recipe_refs')} padded />

				<FieldContainer padded>
					<Label above={true}>Ingredients</Label>
					{ ingredients && ingredients.length > 0 && <IngredientsEdit ingredients={ingredients} onChange={this.handleChange('ingredients')} /> }
					<Button className="add-ingredient" onClick={this.addIngredient}>Add ingredient</Button>
				</FieldContainer> 
				
				<FieldContainer padded>
					<Label above={true}>Method steps</Label>
					{ methods && methods.length > 0 && <MethodsEdit methods={methods} onChange={this.handleChange('methods')} /> }
					<Button className="add-method" onClick={this.addMethod}>Add method</Button>
				</FieldContainer>
				<IconButton className="save" onClick={this.save}>done</IconButton>
			</StyledRecipeEdit>
		);
	}
}

RecipeEdit.propTypes = {
	_id: PropTypes.string,
	photo: PropTypes.string,
	name: PropTypes.string,
	brief_description: PropTypes.string,
	time: PropTypes.string,
	category: PropTypes.string,
	serves: PropTypes.string,
	tags: PropTypes.array,
	personal_notes: PropTypes.string,
	ingredients: PropTypes.array,
	methods: PropTypes.array,
	recipe_refs: PropTypes.string,
	history: PropTypes.any,
	saveRecipe: PropTypes.func,
	returnUrl: PropTypes.string,
};

export default withRouter(RecipeEdit);
