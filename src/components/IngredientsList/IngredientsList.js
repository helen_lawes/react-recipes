import React from 'react';
import PropTypes from 'prop-types';

const Ingredient = ({ amount, unit, name, info }) => (
	<div className="ingredient">
		{amount}{unit} {name} {info}
	</div>
);

Ingredient.propTypes = {
	amount: PropTypes.number,
	unit: PropTypes.string,
	name: PropTypes.string,
	info: PropTypes.string,
};

const groups = ingredients => [...new Set(ingredients.map(ingredient => ingredient.group))];
const groupIngredients = (ingredients, groupName) => ingredients.filter(({ group }) => group === groupName);

const IngredientsList = ({ ingredients }) => (
	<React.Fragment>
		{ingredients.length > 0 && 
			<div className="ingredients">
				<div className="divider"></div>
				<h3>Ingredients</h3>
				{groups(ingredients).map((groupName, i) => (
					<React.Fragment key={i}>
						{groupName && <h4>For the {groupName}</h4>}
						{groupIngredients(ingredients, groupName).map( (ingredient, i) => <Ingredient {...ingredient} key={i} />)}
					</React.Fragment>
				))}
			</div>
		}
	</React.Fragment>
);
IngredientsList.propTypes = {
	ingredients: PropTypes.array.isRequired,
};

export default IngredientsList;
