import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { getColor } from '../../utils/styles';
import StyledButton from '../Buttons/Button.styles';

import FieldContainer from './FieldContainer';
import Label from './Label';

const Wrapper = styled.div`
	position: relative;
	input {
		position: absolute;
		left: -9999px;
	}
	.value {
		display: inline-block;
		max-width: calc(100% - 150px);
		text-overflow: ellipsis;
		overflow: hidden;
		white-space: nowrap;
		vertical-align: middle;
		font-size: 14px;
		color: ${getColor('secondary-text')};
	}
`;

const ChooseFileButton = StyledButton.withComponent('label');

const File = ({ className, label, value, padded, id, ...otherProps }) => (
	<FieldContainer className={className} padded={padded}>
		{label && <Label above={true}>{label}</Label>}
		<Wrapper>
			<input id={id} type="file" {...otherProps} />
			<ChooseFileButton htmlFor={id}>Choose file</ChooseFileButton>
			<div className="value">{value}</div>
		</Wrapper>
	</FieldContainer>
);

File.propTypes = {
	className: PropTypes.string,
	label: PropTypes.string,
	value: PropTypes.string,
	padded: PropTypes.bool,
	id: PropTypes.string,
};

export default File;
