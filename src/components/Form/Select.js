import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { placeholders } from '../../utils/styles';

import FieldContainer from './FieldContainer';

const StyledSelect = styled.select`
	${placeholders.input}
	width: 100%;
`;

const Select = ({ children, className, ...otherProps }) => (
	<FieldContainer className={className}>
		<StyledSelect {...otherProps}>
			{children}
		</StyledSelect>
	</FieldContainer>
);

Select.propTypes = {
	children: PropTypes.node,
	className: PropTypes.string,
};

export default Select;
