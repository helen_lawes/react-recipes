import styled from 'styled-components';
import { placeholders, getColor } from '../../utils/styles';

const Input = styled.input`
	${placeholders.input}
	width:100%;
	${props => props.large && `
		font-size: 27px;
		height: 40px;
	`}
	${props => props.light && `
		color: ${getColor('light')};
	`}
	${props => props.error && `
		color: ${getColor('accent-dark')};
		border-bottom-color: ${getColor('accent-dark')};
	`}
`;

export default Input;
