import React from 'react';
import PropTypes from 'prop-types';

import FieldContainer from './FieldContainer';
import Label from './Label';
import Input from './Input';
import Textarea from './Textarea';

const TextField = ({ value, onChange, type, className, label, id, placeholder, describedby, multiline = false, padded = false, large = false, light = false, error = false }) => (
	<FieldContainer className={className} padded={padded}>
		{ label && <Label htmlFor={id} above={!!value} large={large}>{label}</Label> }
		{ multiline ? 
			<Textarea value={value} onChange={onChange} id={id} placeholder={placeholder} light={light} error={error} aria-describedby={describedby} aria-invalid={error} /> 
			: 
			<Input value={value} type={type} onChange={onChange} id={id} large={large} placeholder={placeholder} light={light} error={error} aria-describedby={describedby} aria-invalid={error} /> 
		}
	</FieldContainer>
);

TextField.propTypes = {
	value: PropTypes.string.isRequired,
	onChange: PropTypes.func.isRequired,
	type: PropTypes.string,
	className: PropTypes.string,
	label: PropTypes.string,
	id: PropTypes.string,
	describedby: PropTypes.string,
	placeholder: PropTypes.string,
	multiline: PropTypes.bool,
	padded: PropTypes.bool,
	large: PropTypes.bool,
	light: PropTypes.bool,
	error: PropTypes.bool,
};

export default TextField;
