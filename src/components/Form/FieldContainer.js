import styled from 'styled-components';

const FieldContainer = styled.div`
	position: relative;
	width: 100%;
	margin: ${props => props.padded ? '35px 0 25px' : '0'};
`;

export default FieldContainer;
