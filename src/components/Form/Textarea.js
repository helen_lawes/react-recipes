import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { placeholders, getColor } from '../../utils/styles';

const StyledTextarea = styled.textarea`
	${placeholders.input}
	width:100%;
	overflow: hidden;
	resize: none;
	${props => props.light && `
		color: ${getColor('light')};
	`}
	${props => props.error && `
		color: ${getColor('accent-dark')};
		border-bottom-color: ${getColor('accent-dark')};
	`}
`;

class Textarea extends React.Component {
	constructor() {
		super();
		this.ref = React.createRef();
		this.handleChange = this.handleChange.bind(this);
	}
	
	componentDidMount() {
		setTimeout(() => {
			this.resize();
		}, 0);
	}

	shouldComponentUpdate(nextProps) {
		return this.props.value !== nextProps.value;
	}

	componentDidUpdate() {
		setTimeout(() => {
			this.resize();
		}, 0);
	}

	handleChange(event) {
		this.resize();
		this.props.onChange(event);
	}

	resize() {
		this.ref.current.style.height = '';
		this.ref.current.style.height = `${this.ref.current.scrollHeight}px`;
	}

	render() {
		return <StyledTextarea {...this.props} onChange={this.handleChange} innerRef={this.ref} />;
	}
}

Textarea.propTypes = {
	onChange: PropTypes.func.isRequired,
	value: PropTypes.string,
};

export default Textarea;
