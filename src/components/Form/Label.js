import styled from 'styled-components';
import { placeholders, getColor } from '../../utils/styles';

const Label = styled.label`
	${placeholders.transition}
	display: block;
	color: ${getColor('secondary-text')};
	position: absolute;
	transform-origin: left;
	transform: ${props => props.above ? ( props.large ? 'translate(0, -25px) scale(0.45)' : 'translate(0,-20px) scale(0.75)') : 'translate(0,4px) scale(1)'};
	pointer-events: none;
	${props => props.large && `
		font-size: 27px;
		height: 40px;
	`}
`;

export default Label;
