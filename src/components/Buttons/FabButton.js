import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import { placeholders, getColor } from '../../utils/styles';

const StyledFabButton = styled.button`
	${placeholders.boxShadow}
	${placeholders.transition}
	cursor: pointer;
	width:56px;
	height:56px;
	line-height: 56px;
	font-size: 24px;
	text-align: center;
	font-family: 'Material Icons';
	border-radius:100%;
	background:${getColor('accent')};
	color:${getColor('icon')};
	display: inline-block;
	text-rendering: optimizeLegibility;
	font-feature-settings: "liga" 1;
	&.snack-open{
		transform:translate(0,-50%);
	}
	&.fab-mini{
		background:${getColor('light')};
		color:${getColor('secondary-text')};
		line-height: 40px;
		width: 40px;
		height: 40px;
	}
	&:focus,
	&:hover{
		${placeholders.elevatedBoxShadow}
	}
	${props => props.fixed ? `
		position: fixed;
	`: ''}
	${props => props.bottomCorner ? `
		right: 30px;
		bottom: 30px;
	`: ''}
`;

const FabButton = ({ children, to, history, ...otherProps }) => (
	<StyledFabButton {...otherProps} onClick={()=>history.push(to)}>{children}</StyledFabButton>
);

FabButton.propTypes = {
	children: PropTypes.node,
	to: PropTypes.string,
	history: PropTypes.any,
};

export default withRouter(FabButton);
