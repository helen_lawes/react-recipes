import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';

const StyledIconButton = styled.button`
	background: none;
`;

const onClickEvent = (onClick, to, history) => {
	if (onClick) return { onClick };
	if (to) return { onClick: () => history.push(to) };
	return {};
};

const IconButton = ({ children, className, to, onClick, history, ...otherProps }) => (
	<StyledIconButton {...otherProps} className={`icon ${className}`} {...onClickEvent(onClick, to, history)}>{children}</StyledIconButton>
);

IconButton.propTypes = {
	children: PropTypes.node,
	className: PropTypes.string,
	to: PropTypes.string,
	onClick: PropTypes.func,
	history: PropTypes.any,
};

export default withRouter(IconButton);
