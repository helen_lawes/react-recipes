import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import StyledButton from './Button.styles';

const onClickEvent = (onClick, to, history) => {
	if (onClick) return { onClick };
	if (to) return { onClick: () => history.push(to) };
	return {};
};

const Button = ({ children, to, onClick, history, ...otherProps }) => (
	<StyledButton {...otherProps} {...onClickEvent(onClick, to, history)}>{children}</StyledButton>
);

Button.propTypes = {
	children: PropTypes.node,
	to: PropTypes.string,
	onClick: PropTypes.func,
	history: PropTypes.any,
};

export default withRouter(Button);
