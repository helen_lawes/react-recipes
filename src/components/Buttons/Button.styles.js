import styled from 'styled-components';
import { placeholders, getColor } from '../../utils/styles';

const StyledButton = styled.button`
	${placeholders.fontNormal}
	text-transform: uppercase;
	font-size: 14px;
	padding:10px;
	border-radius:4px;
	display: inline-block;
	cursor: pointer;
	color:${getColor('accent')};
	transition: 0.25s;
	margin-right:10px;
	vertical-align: middle;
	background: none;
	${props => props.raised && `
		${placeholders.boxShadow}
		${placeholders.transition}
		&:focus,
		&:hover{
			${placeholders.elevatedBoxShadow}
		}
	`}
	${props => props.accent && `
		background:${getColor('accent')};
		color:${getColor('icon')};
	`}
	${props => props.primary && `
		background:${getColor('primary-light')};
	`}
	${props => props.wide && `
		width:100%;
		margin-bottom:15px;
		display: block;
	`}
	&:focus,
	&:hover{
		color:${getColor('accent')};
		background:${getColor('hint')};
	}
`;
export default StyledButton;
