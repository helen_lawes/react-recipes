import React from 'react';
import PropTypes from 'prop-types';
import bindMethods from 'yaab';

import { TextField, Select } from '../Form';
import { IconButton } from '../Buttons';
import StyledIngredient from './Ingredient.styles';

class Ingredient extends React.Component {
	constructor() {
		super();
		this.state = {
			amount: '',
			unit: '',
			name: '',
			info: '',
			group: '',
		};
		bindMethods(this);
	}

	componentDidMount() {
		let { amount = '', unit = '', name = '', info = '', group = '' } = this.props;
		this.setState({ amount, unit, name, info, group });
	}

	handleChange(name) {
		return event => {
			this.setState({
				[name]: event.target.value,
			}, () => {
				this.props.onChange(this.state);
			});
		};
	}

	render() {
		let { amount, unit, name, info, group } = this.state;
		return (
			<StyledIngredient>
				<TextField type="number" className="amount" value={amount} placeholder="0" onChange={this.handleChange('amount')} />
				<Select className="unit" value={unit} onChange={this.handleChange('unit')}>
					<option value="">(No Unit)</option>
					<optgroup label="Weight/Mass">
						<option value="g">grams (g)</option>
						<option value="kg">kilograms (kg)</option>
						<option value="oz">ounces (oz)</option>
						<option value="lb">pounds (lb)</option>
					</optgroup>
					<optgroup label="Volume">
						<option value="ml">millilitres (ml)</option>
						<option value="l">litres (l)</option>
						<option value="fl oz">fluid ounces (fl oz)</option>
						<option value=" pt">pint(s)</option>
						<option value=" tsp">teaspoon (tsp)</option>
						<option value=" dsp">dessertspoon (dsp)</option>
						<option value=" tbsp">tablespoon (tbsp)</option>
						<option value=" cup">cup(s)</option>
						<option value=" pinch">pinch</option>
					</optgroup>
				</Select>
				<TextField className="name" value={name} placeholder="name" onChange={this.handleChange('name')} />
				<TextField className="info" value={info} placeholder="info" onChange={this.handleChange('info')} />
				<TextField className="group" value={group} placeholder="group" onChange={this.handleChange('group')} />
				<IconButton className="delete" onClick={this.props.onDelete}>delete</IconButton>
			</StyledIngredient>
		);
	}
}

Ingredient.propTypes = {
	amount: PropTypes.string,
	unit: PropTypes.string,
	name: PropTypes.string,
	info: PropTypes.string,
	group: PropTypes.string,
	onChange: PropTypes.func,
	onDelete: PropTypes.func,
};

export default Ingredient;
