import React from 'react';
import PropTypes from 'prop-types';
import bindMethods from 'yaab';

import Ingredient from './Ingredient';

class IngredientsEdit extends React.Component {
	constructor() {
		super();
		this.state = {
			ingredients: []
		};
		bindMethods(this);
	}

	static getDerivedStateFromProps(props) {
		let { ingredients } = props;
		return { ingredients };
	}

	handleChange(index) {
		return value => {
			this.setState(({ ingredients }) => {
				ingredients[index] = value;
				return {
					ingredients,
				};
			}, () => {
				this.props.onChange(this.state.ingredients);
			});
		};
	}
	
	handleDelete(index) {
		return event => {
			event.preventDefault();
			this.setState(({ ingredients }) => {
				ingredients.splice(index, 1);
				return {
					ingredients,
				};
			}, () => {
				this.props.onChange(this.state.ingredients);
			});
		};
	}

	render() {
		let { ingredients } = this.state;
		return (
			<React.Fragment>
				{ ingredients && ingredients.map( (ingredient, i) => <Ingredient {...ingredient} key={i} onChange={this.handleChange(i)} onDelete={this.handleDelete(i)} />) }
			</React.Fragment>
		);
	}
}

IngredientsEdit.propTypes = {
	ingredients: PropTypes.array,
	onChange: PropTypes.func,
};

export default IngredientsEdit;
