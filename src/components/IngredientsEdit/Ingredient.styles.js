import styled from 'styled-components';

import { media } from '../../utils/styles';


const StyledIngredient = styled.div`
	display: flex;
	> div {
		width: ${(1/5)*100}%;
		margin-right: 10px;
		${media.small`
			margin-right: 0;
			margin-top: 10px;
			width: 100%;
			&.amount, &.unit{
				width: 50%;
				display: inline-block;
			}
		`}
	}
	.delete{
		order:6;
		${media.small`
			position: absolute;
			top: 50%;
			right: -30px;
		`}
	}
	.amount{
		display: inline-block;
		order:1;
	}
	.unit{
		order:2;
	}
	.name{
		order:3;
	}
	.info{
		order:4;
	}
	.group{
		order:5;
	}
	${media.small`
		display: block;
		margin-bottom: 45px;
		position: relative;
		width: calc(100% - 25px);
	`}
`;

export default StyledIngredient;
