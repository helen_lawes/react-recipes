import styled from 'styled-components';

import { mixins, getColor } from '../../utils/styles';

const StyledActionBar = styled.div`
	box-shadow: 0 2px 5px rgba(0,0,0,0.26);
	background:${getColor('primary')};
	padding: 20px 25px;
	color: ${getColor('light')};
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	z-index: 4;
	display:flex;
	.icon{
		cursor: pointer;
	}
	span{
		display: inline-block;
		vertical-align: middle;
	}
	.pull-right{
		float: right;
	}
	h1{
		font-size: 16px;
		display: inline-block;
		margin:0 0 0 20px;
		width:100%;
		text-transform: capitalize;
	}
	.icon{
		vertical-align: bottom;
		&.active{
			color:${getColor('accent-light')};
		}
	}
	.search{
		height: 23px;
		display: none;
		width:calc(100% - 44px);
		margin:0 20px;
		input{
			color:${getColor('light')};
			height:auto;
			width:100%;
			font-size: 16px;
			border-bottom: none;
			&:focus{
				margin-bottom: -1px;
			}
		}
		inline-input{
			position: relative;
			top:-2px;
			width:calc(100% - 30px);
		}
		&.visible{
			display: inline-block;
		}
	}
	${mixins.placeholder`
		color:${getColor('primary-light')};
	`}
`;

export default StyledActionBar;
