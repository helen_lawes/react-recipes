import React from 'react';
import PropTypes from 'prop-types';

import StyledActionBar from './ActionBar.styles';

const ActionBar = ({ children }) => (
	<StyledActionBar>{children}</StyledActionBar>
);

ActionBar.propTypes = {
	children: PropTypes.node,
};

export default ActionBar;
