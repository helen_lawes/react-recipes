import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { hex2Rgba, media, placeholders, getColor } from '../../utils/styles';
import StyledButton from '../../components/Buttons/Button.styles';
import Cooking from '../../media/cooking.jpg';

const Background = styled.div`
	background:#333 url(${Cooking}) center;
	background-size:cover;
	color: #fff;
	margin:-10px;
	min-height:100vh;
	overflow: auto;
`;

const StyledLogin = styled.div`
	background: ${hex2Rgba('#000', 0.5)};
	padding: 20px;
	width: 100%;
	max-width: 880px;
	margin: 50px auto;
	text-align: center;
	${media.small`
		margin-top: 0;
		margin-bottom: 0;
		min-height: 100vh;
	`}
`;

const StyledForm = styled.form`
	max-width:400px;
	width:100%;
	margin:0 auto;
`;

export const Form = ({ children, ...otherProps}) => (
	<Background>
		<StyledLogin>
			<StyledForm {...otherProps}>{children}</StyledForm>
		</StyledLogin>
	</Background>
);

Form.propTypes = {
	children: PropTypes.node,
};

export const SignInButton = StyledButton.extend`
	margin-top: 40px;
`;

export const Heading = styled.h1`
	${placeholders.cursiveFont}
	font-size: 40px;
`;

export const Error = styled.div`
	color: ${getColor('accent-dark')};
`;
