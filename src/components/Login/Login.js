import React from 'react';
import PropTypes from 'prop-types';
import bindMethods from 'yaab';

import { TextField } from '../../components/Form';
import { Form, SignInButton, Heading, Error } from './Login.styles';
import { Button } from '../Buttons';
import Logo from '../Logo/Logo';

class Login extends React.Component {
	constructor() {
		super();
		this.state = {
			email: '',
			emailConfirm: '',
			password: '',
			passwordConfirm: '',
			formSubmitted: false,
		};
		bindMethods(this);
	}

	handleChange(name) {
		return event => {
			let value = event.target ? event.target.value : event;
			this.setState({
				[name]: value,
			});
			this.clearExternalErrors();
		};
	}

	clearExternalErrors() {
		if (this.props.errors.length) {
			this.props.clearErrors();
		}
	}

	showErrors() {
		return !!(this.state.formSubmitted || this.props.errors.length);
	}

	hasError(field) {
		let match = field.match(/confirm/i) ? field.replace(/confirm/i,'') : null;
		return !this.state[field] || (match && this.state[field] !== this.state[match]);
	}
	
	valid() {
		if (this.hasError('email') || this.hasError('password')) return false;
		if (this.state.register && (this.hasError('emailConfirm') || this.hasError('passwordConfirm'))) return false;
		return true;
	}

	submit(e) {
		e.preventDefault();
		this.setState({
			formSubmitted: true,
		});
		if (this.valid()) {
			let { email, password, emailConfirm, passwordConfirm } = this.state;
			this.props.submit({ email, password, emailConfirm, passwordConfirm });
		}
	}

	render() {
		let { email, emailConfirm, password, passwordConfirm } = this.state;
		let { register, errors } = this.props;
		return (
			<Form onSubmit={this.submit} autoComplete="off">
				<Logo light height="80" /> 
				<Heading>Recipe Book</Heading>

				{errors && 
					<Error role="alert">
						{errors.map((e, i) => <div key={i}>{e.message}</div>)}
					</Error>
				}

				<TextField value={email} label="Email" id="email" describedby="email-error" onChange={this.handleChange('email')} error={this.showErrors() && this.hasError('email')} padded light />
				{this.showErrors() && this.hasError('email') && 
					<Error id="email-error">Please fill in an email</Error>
				}

				{register && 
					<TextField value={emailConfirm} label="Confirm Email" id="emailConfirm" describedby="emailConfirm-error" onChange={this.handleChange('emailConfirm')} error={this.showErrors() && this.hasError('emailConfirm')} padded light /> 
				}
				{register && this.showErrors() && this.hasError('emailConfirm') && 
					<Error id="emailConfirm-error">
						{!emailConfirm ? 'Please confirm email' : 'Email and confirm don\'t match'}
					</Error>
				}
				
				<TextField value={password} type="password" label="Password" id="password" describedby="password-error" onChange={this.handleChange('password')} error={this.showErrors() && this.hasError('password')} padded light />
				{this.showErrors() && this.hasError('password') && 
					<Error id="password-error">Please fill in a password</Error>
				}

				{register && 
					<TextField value={passwordConfirm} type="password" label="Confirm Password" id="passwordConfirm" describedby="passwordConfirm-error" onChange={this.handleChange('passwordConfirm')} error={this.showErrors() && this.hasError('passwordConfirm')} padded light />
				}
				{register && this.showErrors() && this.hasError('passwordConfirm') && 
					<Error id="passwordConfirm-error">
						{!passwordConfirm ? 'Please confirm password' : 'Password and confirm dont\'t match'}
					</Error>
				}

				{register ?
					<React.Fragment>
						<SignInButton accent wide>Register</SignInButton>
						<Button to="/login" wide>Login</Button>
					</React.Fragment>
					:
					<React.Fragment>
						<SignInButton accent wide>Login</SignInButton>
						<Button to="/register" wide>Register</Button>
					</React.Fragment>
				}
			</Form>
		);
	}
}

Login.propTypes = {
	submit: PropTypes.func,
	register: PropTypes.bool,
	errors: PropTypes.array,
	clearErrors: PropTypes.func,
};


export default Login;
