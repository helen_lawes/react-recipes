import recipeApi from '../api/recipes';
import userApi from '../api/user';
import history from '../utils/history';

export const addRecipe = payload => ({
	type: 'ADD_RECIPE',
	payload,
});

export const recipeUpdated = payload => ({
	type: 'UPDATE_RECIPE',
	payload,
});

export const recipeDeleted = payload => ({
	type: 'DELETE_RECIPE',
	payload,
});

export const errorOccurred = payload => ({
	type: 'ADD_ERROR',
	payload,
});

export const clearErrorGroup = payload => ({
	type: 'CLEAR_GROUP_ERRORS',
	payload,
});

const handleErrors = (group, dispatch) => response => {
	// If the api is returning unathorised errors then redirect to the login page
	// if not on the login or register page
	if (response.status >= 400 && response.status <= 499) {
		let pathname = history.location.pathname;
		if (!pathname.match('login') && !pathname.match('register')) {
			history.push('/login');
		}
	} else {
		let _id = new Date().getTime();
		response.text().then(message => {
			dispatch(errorOccurred({
				_id,
				group,
				message,
			}));
		}).catch(() => {
			dispatch(errorOccurred({
				_id,
				group,
				message: 'Unknown error',
			}));
		});
	}
};

const saveToken = token => {
	window.localStorage.setItem('token', token);
	recipeApi.updateToken(token);
};

export const getAllRecipes = () => dispatch => {
	return recipeApi.getAll()
		.then(recipes => {
			recipes.forEach(recipe => dispatch(addRecipe(recipe)));
		})
		.catch(handleErrors('recipes', dispatch));
};

export const updateRecipe = recipe => dispatch => {
	recipe.timestamp = new Date().getTime();
	return recipeApi.update(recipe._id, recipe)
		.then(dispatch(recipeUpdated(recipe)))
		.catch(handleErrors('recipes', dispatch));
};

export const createRecipe = recipe => dispatch => {
	recipe.timestamp = new Date().getTime();
	return recipeApi.create(recipe)
		.then(sRecipe => dispatch(addRecipe(sRecipe)))
		.catch(handleErrors('recipes', dispatch));
};

export const deleteRecipe = recipe => dispatch => {
	return recipeApi.remove(recipe._id)
		.then(() => {
			dispatch(recipeDeleted(recipe));
			history.push('/');
		})
		.catch(handleErrors('recipes', dispatch));
};

export const loginUser = user => dispatch => {
	return userApi.login(user)
		.then(({ token }) => {
			saveToken(token);
			dispatch(clearErrorGroup('user'));
			dispatch(getAllRecipes());
			history.push('/');
		})
		.catch(handleErrors('user', dispatch));
};

export const registerUser = user => dispatch => {
	return userApi.register(user)
		.then(() => {
			history.push('/login');
			dispatch(clearErrorGroup('user'));
		})
		.catch(handleErrors('user', dispatch));
};
