import styled, { injectGlobal } from 'styled-components';

import { placeholders, getColor } from './utils/styles';

injectGlobal`
*{
	box-sizing: border-box;
}
html{
	padding:0;
	margin:0;
	overflow-x:hidden;
}
body{
	font:normal 16px/normal 'Open Sans', sans-serif;
	background:${getColor('hint')};
	padding:10px;
	margin:0;
	overflow-x:hidden;
}
`;

const StyledApp = styled.div`
	img{
		max-width: 100%;
	}
	h1,h2,h3,h4{
		${placeholders.fontNormal}
	}
	a{
		color:${getColor('accent')};
		text-decoration: none;
	}
	.divider{
		margin:20px 0;
		border-top:1px solid ${getColor('divider')};
	}
	button{
		border:none;
	}
	.icon{
		line-height: 22px;
		font-size: 24px;
		width:24px;
		height:24px;
		text-align: center;
		font-family: 'Material Icons';
		text-transform: none;
		color:inherit;
		display: inline-block;
		padding:0;
		text-rendering: optimizeLegibility;
		font-feature-settings: "liga" 1;
	}
`;

export default StyledApp;

