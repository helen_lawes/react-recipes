import Api from './Api';

class Recipes extends Api {
	getAll() {
		return this.fetch();
	}
	get(id) {
		return this.fetch(`/${id}`);
	}
	create(data) {
		return this.post('', this._toFormObject(data));
	}
	update(id, data) {
		return this.put(`/${id}`, this._toFormObject(data));
	}
	remove(id) {
		return this.delete(`/${id}`);
	}
	_toFormObject(data) {
		let file = data.file;
		delete data.file;
		var formData = new FormData();
		formData.append('recipe', JSON.stringify(data));
		formData.append('file', file);
		return formData;
	}
}

const contentTypes = {
	post: null,
	put: null,
};

const responseTypes = {
	delete: null,
};

export default new Recipes('/api/recipes', contentTypes, responseTypes, window.localStorage.getItem('token'));
