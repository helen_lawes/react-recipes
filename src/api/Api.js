export default class Api {
	constructor(baseUrl = '', contentTypes = {}, responseType = {}, token = null) {
		this.baseUrl = baseUrl;
		this.authorization = token;
		this.contentType = {
			default: 'application/json',
			...contentTypes,
		};
		this.responseType = {
			default: 'json',
			...responseType,
		};
	}

	updateToken(token) {
		this.authorization = token;
	}

	fetch(url = '') {
		let opts = {};
		if (this.authorization) {
			opts.headers = {
				'Authorization': `Bearer ${this.authorization}`,
			};
		}
		return fetch(`${this.baseUrl}${url}`, opts)
			.then(response => this._convertResponse('get', response));
	}

	_getContentType(method) {
		let contentType = method in this.contentType ? this.contentType[method] : this.contentType.default;
		if (contentType) {
			return {
				'Content-Type': contentType,
			};
		}
		return {};
	}

	_convertResponse(method, response) {
		let responseType = method in this.responseType ? this.responseType[method] : this.responseType.default;
		if (!response.ok) throw response;
		if (responseType && responseType in response && typeof response[responseType] === 'function') return response[responseType]();
		return response;
	}

	_method(method, url, body) {
		let Authorization = this.authorization ? `Bearer ${this.authorization}` : null;
		return fetch(`${this.baseUrl}${url}`, {
			method,
			headers: {
				Authorization,
				...this._getContentType(method),
			},
			body,
		}).then(response => this._convertResponse(method, response));
	}

	post(...args) {
		return this._method('post', ...args);
	}

	put(...args) {
		return this._method('put', ...args);
	}

	delete(url, data = false) {
		let body = data ? JSON.stringify(data) : null;
		return this._method('delete', url, body);
	}

}
