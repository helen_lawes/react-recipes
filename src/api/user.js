import Api from './Api';

class User extends Api {
	login(userDetails) {
		return this.post('/login', JSON.stringify(userDetails));
	}
	
	register(userDetails) {
		return this.post('/register', JSON.stringify(userDetails));
	}
}

export default new User('/api');
