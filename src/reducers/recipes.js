const initialState = [];

const addRecipe = (state, action) => {
	let recipe = action.payload;
	return [
		...state,
		recipe,
	];
};

const updateRecipe = (state, action) => {
	let recipe = action.payload;
	return state.map(r => {
		if (r._id === recipe._id) return { ...r, ...recipe };
		else return r;
	});
};

const deleteRecipe = (state, action) => {
	let recipe = action.payload;
	return state.filter(r => r._id !== recipe._id);
};

const handlers = {
	'ADD_RECIPE': addRecipe,
	'UPDATE_RECIPE': updateRecipe,
	'DELETE_RECIPE': deleteRecipe,
};

const recipesReducer = (state = initialState, action) => {
	if (handlers.hasOwnProperty(action.type)){
		return handlers[action.type](state, action);
	} else {
		return state;
	}
};

export default recipesReducer;
