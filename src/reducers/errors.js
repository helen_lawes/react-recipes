const initialState = [];

const addError = (state, action) => {
	let error = action.payload;
	return [
		...state,
		error,
	];
};

const deleteError = (state, action) => {
	let error = action.payload;
	return state.filter(e => e._id !== error._id);
};

const clearGroupErrors = (state, action) => {
	let group = action.payload;
	return state.filter(e => e.group !== group);
};

const clearErrors = () => {
	return initialState;
};

const handlers = {
	'ADD_ERROR': addError,
	'DELETE_ERROR': deleteError,
	'CLEAR_GROUP_ERRORS': clearGroupErrors,
	'CLEAR_ERRORS': clearErrors,
};

const errorsReducer = (state = initialState, action) => {
	if (handlers.hasOwnProperty(action.type)){
		return handlers[action.type](state, action);
	} else {
		return state;
	}
};

export default errorsReducer;
