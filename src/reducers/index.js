import { combineReducers } from 'redux';

import recipeReducer from './recipes';
import errorsReducer from './errors';

export default combineReducers({
	recipes: recipeReducer,
	errors: errorsReducer,
});


